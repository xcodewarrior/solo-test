# Solo Test Gizlilik Politikası
## Son güncelleme 11/8/2021

Solo Test ("biz" veya "biz" veya "bizim") kullanıcılarımızın ("kullanıcı" veya "siz") gizliliğine saygı duyar. Bu Gizlilik Politikası, mobil uygulamamızı ("Uygulama") ziyaret ettiğinizde bilgilerinizi nasıl topladığımızı, kullandığımızı, ifşa ettiğimizi ve koruduğumuzu açıklamaktadır. Lütfen bu Gizlilik Politikasını dikkatlice okuyunuz. BU GİZLİLİK POLİTİKASININ ŞARTLARINI KABUL ETMİYORSANIZ, LÜTFEN UYGULAMAYA ERİŞMEYİN.

Bu Gizlilik Politikasında herhangi bir zamanda ve herhangi bir nedenle değişiklik yapma hakkımız saklıdır. Bu Gizlilik Politikasının “Son güncelleme” tarihini güncelleyerek herhangi bir değişiklik hakkında sizi uyaracağız. Güncellemelerden haberdar olmak için bu Gizlilik Politikasını periyodik olarak gözden geçirmeniz önerilir. Söz konusu revize edilmiş Gizlilik Politikasının yayınlandığı tarihten sonra Uygulamayı kullanmaya devam etmeniz halinde, revize edilmiş herhangi bir Gizlilik Politikasındaki değişikliklerden haberdar olduğunuz, bunlara tabi olacağınız ve kabul ettiğiniz kabul edileceksiniz.

### BİLGİLERİNİZİ TOPLAMA

Kullanıcı hakkında herhangi bir bilgi almıyoruz.

Kanunla veya Hakları Korumak İçin

Yasal sürece yanıt vermek, politikalarımızın olası ihlallerini araştırmak veya düzeltmek veya başkalarının haklarını, mülkiyetini ve güvenliğini korumak için hakkınızdaki bilgilerin açıklanmasının gerekli olduğuna inanırsak, bilgilerinizi izin verildiği veya gerektirdiği şekilde paylaşabiliriz. geçerli herhangi bir yasa, kural veya düzenleme. Bu, dolandırıcılıktan korunma ve kredi riskinin azaltılması için diğer kuruluşlarla bilgi alışverişini içerir.

Üçüncü Taraf Hizmet Sağlayıcılar

Bilgilerinizi, ödeme işleme, veri analizi, e-posta teslimi, barındırma hizmetleri, müşteri hizmetleri ve pazarlama yardımı dahil olmak üzere bizim için veya bizim adımıza hizmet veren üçüncü taraflarla paylaşabiliriz.

Pazarlama iletişimi

Onayınızla veya izninizi geri çekme fırsatıyla, bilgilerinizi yasaların izin verdiği şekilde pazarlama amacıyla üçüncü taraflarla paylaşabiliriz.

Diğer Kullanıcılarla Etkileşimler

Uygulamanın diğer kullanıcılarıyla etkileşime girerseniz, bu kullanıcılar adınızı, profil fotoğrafınızı ve diğer kullanıcılara davetiye gönderme, diğer kullanıcılarla sohbet etme, gönderileri beğenme, blogları takip etme dahil olmak üzere etkinliğinizin açıklamalarını görebilir.

Çevrimiçi Gönderiler

Uygulamalara yorum, katkı veya başka içerik gönderdiğinizde, gönderileriniz tüm kullanıcılar tarafından görüntülenebilir ve kalıcı olarak Uygulama dışında herkese açık olarak dağıtılabilir.

Üçüncü Taraf Reklamverenler

Uygulamayı ziyaret ettiğinizde reklam sunmak için üçüncü taraf reklam şirketlerini kullanabiliriz. Bu şirketler, ilginizi çeken mal ve hizmetlerle ilgili reklamlar sunmak için Uygulamaya ve web çerezlerinde yer alan diğer web sitelerine yaptığınız ziyaretlerle ilgili bilgileri kullanabilir.

İştirakler

Bilgilerinizi bağlı şirketlerimizle paylaşabiliriz, bu durumda bu bağlı kuruluşların bu Gizlilik Politikasına uymalarını zorunlu kılacağız. Bağlı kuruluşlar, ana şirketimizi ve kontrol ettiğimiz veya bizimle müşterek kontrol altında olan tüm bağlı kuruluşları, ortak girişim ortaklarını veya diğer şirketleri içerir.

İş ortakları

Size belirli ürünler, hizmetler veya promosyonlar sunmak için bilgilerinizi iş ortaklarımızla paylaşabiliriz.

Teklif duvarı

Uygulama, üçüncü taraflarca barındırılan bir "teklif duvarı" görüntüleyebilir. Bu tür bir teklif duvarı, üçüncü taraf reklamcıların, bir reklam teklifinin kabulü ve tamamlanması karşılığında kullanıcılara sanal para birimi, hediyeler veya başka öğeler sunmasına olanak tanır. Böyle bir teklif duvarı Uygulamada görünebilir ve coğrafi bölgeniz veya demografik bilgileriniz gibi belirli verilere dayalı olarak size gösterilebilir. Bir teklif duvarına tıkladığınızda, Uygulamadan ayrılacaksınız. Kullanıcı kimliğiniz gibi benzersiz bir tanımlayıcı, sahtekarlığı önlemek ve hesabınızı uygun şekilde kredilendirmek için teklif duvarı sağlayıcısıyla paylaşılacaktır.

Sosyal Medya Kişileri

Uygulamaya bir sosyal ağ üzerinden bağlanırsanız, sosyal ağdaki kişileriniz adınızı, profil fotoğrafınızı ve etkinliğinizin açıklamalarını görecektir.

Diğer Üçüncü Taraflar

Genel iş analizi yapmak amacıyla bilgilerinizi reklamcılar ve yatırımcılarla paylaşabiliriz. Bilgilerinizi, yasaların izin verdiği şekilde, pazarlama amacıyla bu tür üçüncü taraflarla da paylaşabiliriz.

Satış veya İflas

Varlıklarımızın tamamını veya bir kısmını yeniden düzenlersek veya satarsak, birleşirsek veya başka bir kuruluş tarafından satın alınırsak, bilgilerinizi halef kuruluşa aktarabiliriz. İşi bırakırsak veya iflas edersek, bilgileriniz üçüncü bir tarafça devredilen veya edinilen bir varlık olacaktır. Bu tür aktarımların gerçekleşebileceğini ve devralanın bu Gizlilik Politikasında verdiğimiz onur taahhütlerini reddedebileceğini kabul edersiniz.

Kişisel veya hassas verilerinizi paylaştığınız üçüncü şahısların eylemlerinden sorumlu değiliz ve üçüncü şahıs taleplerini yönetme veya kontrol etme yetkimiz yok. Artık üçüncü taraflardan yazışma, e-posta veya diğer iletişimleri almak istemiyorsanız, üçüncü tarafla doğrudan iletişim kurmaktan siz sorumlusunuz.

### İZLEME TEKNOLOJİLERİ

Çerezler ve Web İşaretçileri

Herhangi bir çerez ve web işaretçisi kullanmıyoruz

İnternet Tabanlı Reklamcılık

Apple Watch uygulaması için herhangi bir reklam aracı kullanmıyoruz.

Web Sitesi Analitiği

Ayrıca, diğer şeylerin yanı sıra, kullanıcıların kullanımını analiz etmek ve izlemek için birinci taraf çerezleri ve üçüncü taraf çerezleri kullanarak Uygulama üzerinde izleme teknolojilerine ve yeniden pazarlama hizmetlerine izin vermek için Google Analytics gibi seçili üçüncü taraf satıcılarla ortaklık yapabiliriz. Uygulamanın, belirli içeriğin popülerliğini belirleyin ve çevrimiçi etkinliği daha iyi anlayın. Uygulamaya erişerek, bilgilerinizin bu üçüncü taraf satıcılar tarafından toplanmasına ve kullanılmasına izin vermiş olursunuz. Gizlilik politikalarını gözden geçirmeniz ve sorularınıza yanıt almak için doğrudan onlarla iletişime geçmeniz önerilir. Kişisel bilgileri bu üçüncü taraf satıcılara aktarmayız.

Yeni bir bilgisayar almanın, yeni bir tarayıcı kurmanın, mevcut bir tarayıcıyı yükseltmenin veya tarayıcınızın çerez dosyalarını silmenin veya başka bir şekilde değiştirmenin de belirli devre dışı bırakma çerezlerini, eklentileri veya ayarları temizleyebileceğini bilmelisiniz.

### ÜÇÜNCÜ TARAF WEB SİTELERİ

Herhangi bir 3. parti web sitesi kullanmıyoruz.

### BİLGİLERİNİN GÜVENLİĞİ

Kişisel bilgilerinizin korunmasına yardımcı olmak için idari, teknik ve fiziksel güvenlik önlemleri kullanıyoruz. Bize sağladığınız kişisel bilgilerin güvenliğini sağlamak için makul adımlar atmış olsak da, tüm çabalarımıza rağmen hiçbir güvenlik önleminin mükemmel veya aşılmaz olmadığını ve herhangi bir müdahaleye veya başka türden kötüye kullanıma karşı hiçbir veri aktarım yönteminin garanti edilemeyeceğini lütfen unutmayın. Çevrimiçi olarak ifşa edilen herhangi bir bilgi, yetkisiz taraflarca ele geçirilmeye ve kötüye kullanılmaya açıktır. Bu nedenle, kişisel bilgilerinizi vermeniz durumunda tam güvenliği garanti edemeyiz.

### ÇOCUKLAR İÇİN POLİTİKA

13 yaşın altındaki çocuklardan bilerek bilgi talep etmiyoruz veya bu çocuklara pazarlama yapmıyoruz. 13 yaşın altındaki çocuklardan topladığımız herhangi bir veriden haberdar olursanız, lütfen aşağıda verilen iletişim bilgilerini kullanarak bizimle iletişime geçin.

### TAKİP ETMEYİN ÖZELLİKLERİ İÇİN KONTROLLER

Çoğu web tarayıcısı ve bazı mobil işletim sistemleri, çevrimiçi tarama etkinliklerinizle ilgili verilerin izlenmemesi ve toplanmaması için gizlilik tercihinizi belirtmek için etkinleştirebileceğiniz bir Do-Not-Track (“DNT”) özelliği veya ayarı içerir. DNT sinyallerini tanımak ve uygulamak için tek tip bir teknoloji standardı kesinleşmemiştir. Bu nedenle, şu anda DNT tarayıcı sinyallerine veya çevrimiçi izlenmeme tercihinizi otomatik olarak bildiren diğer mekanizmalara yanıt vermiyoruz. Gelecekte izlememiz gereken bir çevrimiçi izleme standardı benimsenirse, bu Gizlilik Politikasının gözden geçirilmiş bir versiyonunda sizi bu uygulama hakkında bilgilendireceğiz.

### KALİFORNİYA GİZLİLİK HAKLARI

“Shine The Light” yasası olarak da bilinen California Medeni Kanunu Bölüm 1798.83, California'da ikamet eden kullanıcılarımızın, yılda bir kez ve ücretsiz olarak bizden kişisel bilgi kategorileri hakkında (varsa) bilgi talep etmesine ve almasına izin verir. doğrudan pazarlama amaçları için üçüncü taraflara ifşa edildi ve hemen önceki takvim yılında kişisel bilgileri paylaştığımız tüm üçüncü tarafların adları ve adresleri. Kaliforniya'da ikamet ediyorsanız ve böyle bir talepte bulunmak istiyorsanız, lütfen aşağıda verilen iletişim bilgilerini kullanarak talebinizi yazılı olarak bize iletin.

18 yaşından küçükseniz, Kaliforniya'da ikamet ediyorsanız ve Uygulamada kayıtlı bir hesabınız varsa, Uygulamada herkese açık olarak yayınladığınız istenmeyen verilerin kaldırılmasını talep etme hakkınız vardır. Bu tür verilerin kaldırılmasını talep etmek için lütfen aşağıda verilen iletişim bilgilerini kullanarak bizimle iletişime geçin ve hesabınızla ilişkili e-posta adresini ve Kaliforniya'da ikamet ettiğinize dair bir beyanı ekleyin. Verilerin Uygulamada herkese açık olarak görüntülenmediğinden emin olacağız, ancak lütfen verilerin sistemlerimizden tamamen veya kapsamlı bir şekilde kaldırılamayacağını unutmayın.

### BİZE ULAŞIN

Bu Gizlilik Politikası hakkında sorularınız veya yorumlarınız varsa, lütfen bizimle şu adresten iletişime geçin:

emrahkrkmz1@gmail.com
